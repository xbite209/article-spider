module github.com/PeterYangs/article-spider/v3

go 1.15

require (
	github.com/360EntSecGroup-Skylar/excelize/v2 v2.3.2
	github.com/PeterYangs/request v0.0.14
	github.com/PeterYangs/tools v0.2.36
	github.com/PuerkitoBio/goquery v1.6.1
	github.com/chromedp/cdproto v0.0.0-20211126220118-81fa0469ad77
	github.com/chromedp/chromedp v0.7.6
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/shopspring/decimal v1.2.0
	golang.org/x/net v0.0.0-20210316092652-d523dce5a7f4
)
